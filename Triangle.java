
/*
This is just an initial skeleton of the class to help you get started. 
It does NOT contain all the methods to complete the assignment requirements.
As you add more code to it, you might have to do more imports. 
*/

public class Triangle extends Shape
{
   	private int height;	// height of isosceles triangle
   	private int rInc;   // only (1, 0), (-1,0), (0,1) and (0,-1)
	private int cInc;   // are allowed


    //define the constructor following the signature in the specification
   	
   	Triangle(int rowBase, int colBase, int height, int rowIncrement,
   			int colIncrement, char drawingCharacter){
   		super(rowBase, colBase, drawingCharacter);
   		this.height = height;
		this.rInc = rowIncrement;
		this.cInc = colIncrement;
   	}

   	public void draw(Window window)
   	{
   		//assuming row position of the base point of this triangle is 'rb'
   		//assuming column position of the base point of this triangle is 'cb'
   		//assuming the drawing character is 'character'
   		//assuming the constructor in the Line class has been defined according to the specification

      	if(rInc == 0)//when the height vector goes right or left from the base point
      	{
			Line line1 = new Line(getRb(), getCb(), height, -1, cInc, getCharacter());
			Line line2 = new Line(getRb(), getCb(), height, 1, cInc, getCharacter()); 
			Line line3 = new Line(getRb() - height, getCb() + cInc * height, 2 *height,
				1, 0, getCharacter());
			//now use the draw method in the Line class to draw the triangle
			line1.draw(window);
			line2.draw(window);
			line3.draw(window);
			
		}
		else if(cInc == 0)//when the height vector goes up or down from the base point
		{
			Line line1 = new Line(getRb(), getCb(), height, rInc, -1, getCharacter());
			Line line2 = new Line(getRb(), getCb(),  height, rInc, 1, getCharacter());
			Line line3 = new Line(getRb() + rInc * height, getCb() - height,
				 2*height, 0, 1, getCharacter());
			//now use the draw method in the Line class to draw the triangle
			line1.draw(window);
			line2.draw(window);
			line3.draw(window);
			
		}
	}
   	
   	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getrInc() {
		return rInc;
	}

	public void setrInc(int rInc) {
		this.rInc = rInc;
	}

	public int getcInc() {
		return cInc;
	}

	public void setcInc(int cInc) {
		this.cInc = cInc;
	}

	//define other methods...

}
