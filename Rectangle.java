
/*
This is just an initial skeleton of the class to help you get started. 
It does NOT contain all the methods to complete the assignment requirements.
As you add more code to it, you might have to do more imports.
*/

public class Rectangle extends Shape {
	private int height;
	private int width;

	// define the constructor following the signature in the specification

	Rectangle(int rowBase, int colBase, int height, int width, char drawingCharacter) {
		super(rowBase, colBase, drawingCharacter);
		this.setHeight(height);
		this.setWidth(width);
	}

	public void draw(Window window) {
		// treat the rectangle as four lines

		// Line line1 = new Line(/*appropriate parameters goes here*/);
		// similarly line2, line3, line4

		Line line1 = new Line(getRb(), getCb(), getHeight(), 1, 0, getCharacter());
		Line line2 = new Line(getRb(), getCb(), getWidth(), 0, 1, getCharacter());
		Line line3 = new Line(getRb() + getHeight(), getCb(), getWidth(), 0, 1, getCharacter());
		Line line4 = new Line(getRb(), getCb() + getWidth(), getHeight(), 1, 0, getCharacter());
		
		
		line1.draw(window);
		line2.draw(window);
		line3.draw(window);
		line4.draw(window);
		// now use the draw method in the Line class to draw the rectangle
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	// define other methods...

}
