

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.io.*;

/*
This is just an initial skeleton of the class to help you get started. 
It does NOT contain all the methods to complete the assignment requirements.
As you add more code to it, you might have to do more imports. 
*/

public class DrawingBoard {
	private static Scanner kb = new Scanner(System.in);
	private static Window w = null;
	private static ArrayList<Shape> shapes = null;
	private static Shape selectedShape = null;
	static Line l = null;
	static int len = 0, col = 0, row = 0;

	public static void main(String[] args) throws Exception {
		// Create or load a window
		// Display the window with grid added
		System.out.println("Enter the window file name (or NEW): ");
		String name = kb.nextLine().trim();
		if (name.equalsIgnoreCase("NEW")) {
			System.out.println("Enter number of rows, number of columns and character (separated by space): ");
			int rbase = kb.nextInt();
			int cbase = kb.nextInt();
			char ch = kb.nextLine().trim().charAt(0);
			w = new Window(rbase, cbase, ch);
		} else {
			// call the appropriate method in the Window class to read the drawing
			// specifications from file
			// and initialize the Window object w
			// you should use the method you developed in Task 4
			w = new Window();
			w = w.readSpecFromFile(name);

		}

		// add the grids using the method you developed in Task 5
		w.addGrid();
		// initialize the shapes ArrayList by calling a get method of the Window class
		shapes = w.shapes;

		// Perform options
		int index = 0;
		boolean repeat = true;

		while (repeat) {
			System.out.println("\n");

			// you may add a call to the refreshImage() method you developed in Task 5
			//w.refreshImage();
			w.display();

			System.out.println("Add Erase Select Write Quit");
			System.out.println("Up Down Left Right + -");

			String cmd = kb.next();
			switch (cmd.toUpperCase().charAt(0)) {
			case 'U':
				if (row > 1) {
					
					index = shapes.indexOf(selectedShape);
					w.refreshImage();
					row -= 1;
					w.addShape(new Line(row, col, len, l.getrInc(), l.getcInc(), l.getCharacter()));
					selectedShape = shapes.get(shapes.size() - 1);
					sortArrayList(index);
				} else
					System.out.println("Can't move up anymore! \nLimit reached.");
				break;
			case 'D':
				
					index = shapes.indexOf(selectedShape);
					w.refreshImage();
					row += 1;
					w.addShape(new Line(row, col, len, l.getrInc(), l.getcInc(), l.getCharacter()));
					selectedShape = shapes.get(shapes.size() - 1);
					sortArrayList(index);
				
				break;
			case 'L':
				if(col>1) {
					index = shapes.indexOf(selectedShape);
					w.refreshImage();
					col -= 1;
					w.addShape(new Line(row, col, len, l.getrInc(), l.getcInc(), l.getCharacter()));
					selectedShape = shapes.get(shapes.size() - 1);
					sortArrayList(index);
				}
				else
					System.out.println("Limit reached");
			
					break;
			case 'R':
				
					index = shapes.indexOf(selectedShape);
					w.refreshImage();
					col += 1;
					w.addShape(new Line(row, col, len, l.getrInc(), l.getcInc(), l.getCharacter()));
					selectedShape = shapes.get(shapes.size() - 1);
					sortArrayList(index);
				
				break;
			case '+':
								
					index = shapes.indexOf(selectedShape);
					w.refreshImage();
					len += 1;
					w.addShape(new Line(row, col, len, l.getrInc(), l.getcInc(), l.getCharacter()));
					selectedShape = shapes.get(shapes.size() - 1);
					sortArrayList(index);
				

				break;

			case '-':
				if (len < 0) {
					System.out.println("The selected shape is removed already! \nPlease select another shape. ");
					break;
				}
				index = shapes.indexOf(selectedShape);
				len -= 1;
				w.addShape(new Line(row, col, len, l.getrInc(), l.getcInc(), l.getCharacter()));
				selectedShape = shapes.get(shapes.size() - 1);
				sortArrayList(index);
				if (len < 0) {
					System.out.println("Shape is erased!");
					shapes.remove(selectedShape);
				}

				break;

			case 'S':
				selectShape();
				break;
			case 'A':
				addShape();
				break;
			case 'E':
				deleteShape();
				break;
			case 'W':
				writeSpecToFile();
				break;
			case 'Q':
				repeat = false;
				System.exit(0);
				break; // quit

			default:
				System.out.println("Wrong option chosen!");
			}
		} // while

		System.out.println("Thank You!");

	}

	// implement all the following methods...

	public static void selectShape() {

		listShapes();
		int i = kb.nextInt();
		selectedShape = shapes.get(i);
		if (!selectedShape.getClass().getSimpleName().equalsIgnoreCase("Line"))
			System.out.println("Please select only the shape line");
		else {
			l = (Line) selectedShape;
			len = l.getLength();
			col = l.getCb();
			row = l.getRb();
		}

	}

	public static void addShape() {
		System.out.println("line rowBase colBase length rowIncrement colIncrement character (separated by space): ");
		String shape = kb.next();
		w.addShape(new Line(kb.nextInt(), kb.nextInt(), kb.nextInt(), kb.nextInt(), kb.nextInt(), kb.next().charAt(0)));

	}

	public static void deleteShape() {

		listShapes();
		int j = kb.nextInt();
		if (!shapes.get(j).getClass().getSimpleName().equalsIgnoreCase("Line"))
			System.out.println("Please select only the shape line");
		else
			sortArrayList(j);
	}

	public static void sortArrayList(int j) {

		int i = 0, l = 0, n = 0;

		if (j <= shapes.size()) {

			shapes.remove(j);
			i = shapes.size();
			w.refreshImage();

			for (int k = 0; k < i; k++) {
				n += 1;
				if (!shapes.isEmpty())
					w.addShape(shapes.get(k));
			}

			for (int m = 0; m < n; m++)
				if (!shapes.isEmpty())
					shapes.remove(l);
		}
	}

	public static void writeSpecToFile() {
		System.out.println("File name: ");
		String fileName = kb.next();
		w.writeSpecToFile(fileName);
	}

	public static void listShapes() {

		int i = 0;
		for (Shape s : shapes)
			System.out.println(i++ + ": " + s);
	}

}
