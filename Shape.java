

import java.io.*;

/*
This is just an initial skeleton of the class to help you get started. 
It does NOT contain all the methods to complete the assignment requirements.
As you add more code to it, you might have to do more imports. 
*/

public abstract class Shape {
	private int rb; // row position of base point
	private int cb; // col position of base point
	private char character; // drawing character

	public Shape() {	};

	public Shape(int rb, int cb, char character) {
		this.setRb(rb);
		this.setCb(cb);
		this.setCharacter(character);
	}

	public abstract void draw(Window window);

	public int getRb() {
		return rb;
	}

	public void setRb(int rb) {
		this.rb = rb;
	}

	public int getCb() {
		return cb;
	}

	public void setCb(int cb) {
		this.cb = cb;
	}

	public char getCharacter() {
		return character;
	}

	public void setCharacter(char character) {
		this.character = character;
	}

	
	
	

	// you might need to add more abstract/concrete methods in this class...
}