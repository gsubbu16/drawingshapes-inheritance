

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/*
This is just an initial skeleton of the class to help you get started. 
It does NOT contain all the methods to complete the assignment requirements.
As you add more code to it, you might have to do more imports.
*/

public class Window {
	protected int rows;
	protected int cols;
	protected ArrayList<Shape> shapes = new ArrayList<>();
	protected char[][] cells;
	protected char border;

	/*
	 * rows = number of rows available for drawing, borders excluded cols = number
	 * of cols available for drawing, borders excluded cells = 2D array of char
	 * depicting the drawing, its size should be: [rows+2][cols+2] shapes = ordered
	 * list of shapes (You can use Arrays, or LinkedList as well if you want)
	 */
	Window() {
	};

	public Window(int rows, int cols, char border) {
		// Initialize everything
		// Make a call to addBorders()
		cells = new char[rows + 2][cols + 2];
		this.rows = rows;
		this.cols = cols;
		this.border = border;
		addBorders(border);
	}

	protected void addBorders(char ch) {
		// add the border using ch as the character
		cells[0][0] = ch;
		cells[0][cols + 1] = ch;
		cells[rows + 1][0] = ch;
		cells[rows + 1][cols + 1] = ch;

		for (int i = 0; i <= rows + 1; i++) {
			for (int j = 0; j <= cols + 1; j++) {
				if (i == 0 || j == 0 || i == rows + 1 || j == cols + 1)
					cells[i][j] = '*';
			}
		}
	}

	public void display() {
		// display the content of the array to the screen

		for (int i = 0; i <= rows + 1; i++) {
			for (int j = 0; j <= cols + 1; j++) {

				System.out.print(cells[i][j] + " ");

			}
			System.out.println();
		}
	}

	public void addShape(Shape shape) {
		// add a shape to the collection
		// call the draw() method of the shape to draw itself on this window
		shapes.add(shape);
		shape.draw(this);
	}

	// This method is needed by classes of type Shape for method draw()
	// It cannot be private
	// We choose it to be accessible at the package level as the safest
	// choice open to us
	void setCell(int row, int col, char ch) {
		// set the character at cells[row][col] to 'ch'
		cells[row][col] = ch;
	}

	void writeSpecToFile(String fileName) {
		Line l = null;
		Circle c = null;
		Rectangle r = null;
		Triangle t = null;
		Text tt = null;

		try (BufferedOutputStream bs = new BufferedOutputStream(new FileOutputStream(fileName))) {
			String fileContent = rows + " " + cols + "\n" + border + "\n" + ".";
			bs.write(fileContent.getBytes());
			for (int i = 0; i < shapes.size(); i++) {

				switch (shapes.get(i).getClass().getSimpleName()) {
				case "Line":
					l = (Line) shapes.get(i);
					fileContent = "\nLine\n" + l.getRb() + " " + l.getCb() + " " + l.getLength() + " " + l.getrInc()
							+ " " + l.getcInc() + "\n" + l.getCharacter() + "\n" + ".";
					break;
				case "Circle":
					c = (Circle) shapes.get(i);
					fileContent = "\nCircle\n" + c.getRb() + " " + c.getCb() + " " + c.getRad() + " " + "\n"
							+ c.getCharacter() + "\n" + ".";
					break;
				case "Rectangle":
					r = (Rectangle) shapes.get(i);
					fileContent = "\nRectangle\n" + r.getRb() + " " + r.getCb() + " " + r.getHeight() + " "
							+ r.getWidth() + " " + "\n" + r.getCharacter() + "\n" + ".";
					break;
				case "Triangle":
					t = (Triangle) shapes.get(i);
					fileContent = "\nTriangle\n" + t.getRb() + " " + t.getCb() + " " + t.getHeight() + " " + t.getrInc()
							+ " " + t.getcInc() + " " + "\n" + t.getCharacter() + "\n" + ".";
					break;
				case "Text":
					tt = (Text) shapes.get(i);
					fileContent = "\nText\n" + tt.getRb() + " " + tt.getCb() + " " + "\n" + tt.getText() + "\n"
							+ tt.getrInc() + " " + tt.getcInc() + "\n" + ".";
					break;

				default:
					System.out.println("Shape not found!");

				}
				bs.write(fileContent.getBytes());
			}

		} catch (IOException e) {
			System.out.println("File not found");
		}

	}

	Window readSpecFromFile(String fileName) throws FileNotFoundException {

		Scanner fileInput = new Scanner(new File(fileName));
		Window w = null;
		Line l = null;
		Circle c = null;
		Rectangle r = null;
		Triangle t = null;
		Text tt = null;

		while (fileInput.hasNextLine()) {
			String line = fileInput.nextLine();

			if (line.charAt(0) != '.') {

				switch (line.toLowerCase()) {
				case "line":
					line = fileInput.nextLine();
					String[] lineSplit = line.split(" ");
					l = new Line(Integer.parseInt(lineSplit[0]), Integer.parseInt(lineSplit[1]),
							Integer.parseInt(lineSplit[2]), Integer.parseInt(lineSplit[3]),
							Integer.parseInt(lineSplit[4]), fileInput.nextLine().charAt(0));
					w.addShape(l);
					break;
				case "circle":
					line = fileInput.nextLine();
					String[] circleSplit = line.split(" ");
					c = new Circle(Integer.parseInt(circleSplit[0]), Integer.parseInt(circleSplit[1]),
							Integer.parseInt(circleSplit[2]), fileInput.nextLine().charAt(0));
					w.addShape(c);
					break;
				case "rectangle":
					line = fileInput.nextLine();
					String[] rectSplit = line.split(" ");
					r = new Rectangle(Integer.parseInt(rectSplit[0]), Integer.parseInt(rectSplit[1]),
							Integer.parseInt(rectSplit[2]), Integer.parseInt(rectSplit[3]),
							fileInput.nextLine().charAt(0));
					w.addShape(r);
					break;
				case "triangle":
					line = fileInput.nextLine();
					String[] triSplit = line.split(" ");
					t = new Triangle(Integer.parseInt(triSplit[0]), Integer.parseInt(triSplit[1]),
							Integer.parseInt(triSplit[2]), Integer.parseInt(triSplit[3]), Integer.parseInt(triSplit[4]),
							fileInput.nextLine().charAt(0));
					w.addShape(t);
					break;
				case "text":
					line = fileInput.nextLine();
					String[] textSplit1 = line.split(" ");
					String textSplit2 = fileInput.nextLine();
					String[] textSplit3 = fileInput.nextLine().split(" ");
					tt = new Text(Integer.parseInt(textSplit1[0]), Integer.parseInt(textSplit1[1]), textSplit2,
							Integer.parseInt(textSplit3[0]), Integer.parseInt(textSplit3[1]));
					w.addShape(tt);
					break;

				default:
					String[] tokens = line.split(" ");
					w = new Window(Integer.parseInt(tokens[0]), Integer.parseInt(tokens[1]),
							fileInput.nextLine().charAt(0));
					break;
				}

			}
		}
		fileInput.close();
		// w.display();
		return w;
	}

	public void addGrid() {
		cells[0][0] = border;
		cells[0][cols + 1] = border;
		cells[rows + 1][0] = border;
		cells[rows + 1][cols + 1] = border;
		char[] a = { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };
		int k = 0, l = 0, m = 0, n = 0;

		// print the left & right lines
		for (int i = 1; i <= rows; i++) {
			for (int j = 0; j <= cols + 2; j++) {
				if (k < a.length || l < a.length) {
					if (k < a.length) {
						if (j == 0) {
							cells[i][j] = a[k];
							k++;
						}
					}
					if (l < a.length) {
						if (j == cols + 1) {
							cells[i][j] = a[l];
							l++;
						}
					}
				} else if (k >= a.length || l >= a.length) {
					k = 0;
					l = 0;
				}
			}
		}
		// print top & bottom lines
		for (int i = 0; i < rows + 2; i++) {
			for (int j = 1; j <= cols; j++) {
				if (m < a.length && n < a.length) {
					if (m < a.length) {
						if (i == 0) {
							cells[i][j] = a[m];
							m++;
						}
					}
					if (n < a.length) {
						if (i == rows + 1) {
							cells[i][j] = a[n];
							n++;
						}
					}
				} else if (m >= a.length || n >= a.length) {
					m = 0;
					if (i == 0) {
						cells[i][j] = a[m];
						m++;
					}
					n = 0;
					if (i == rows + 1) {
						cells[i][j] = a[n];
						n++;
					}
				}
			}

		}
	}

	public void refreshImage() {
		for (int i = 0; i <= rows + 1; i++) {
			for (int j = 0; j <= cols + 1; j++) {
				if (((i != 0 && (j != 0 || j != cols + 1)) && (i != rows + 1 && (j != 0 || j != cols + 1)))
						&& ((j != 0 && (i != 0 || i != rows + 1)) && (j != cols + 1 && (i != 0 || i != rows + 1))))
					cells[i][j] = ' ';
			}
		}
	}

	// define other methods as needed...
}
